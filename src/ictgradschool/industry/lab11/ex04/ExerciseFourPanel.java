package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Balloon balloon;
//    private JButton moveButton;
    private Timer timer;

    private ArrayList<Balloon> balloonslist = new ArrayList<Balloon>();

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon = new Balloon(30, 60);
        balloonslist.add(this.balloon);

//        this.moveButton = new JButton("Move balloon");
//        this.moveButton.addActionListener(this);
//        this.add(moveButton);

        // KeyListener
        this.addKeyListener(this);

        // Timer
        this.timer = new Timer(30,this);

//        // Array
//        this.balloonslist = new ArrayList<Balloon>();

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        for (int i = 0; i < balloonslist.size(); i++) {
            balloonslist.get(i).move();
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        balloon.draw(g);

        for (int i = 0; i < balloonslist.size(); i++) {
            balloonslist.get(i).draw(g);
        }

        requestFocusInWindow(); // Focus the input into the main window
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
            // set direction to up
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            for (int i = 0; i < balloonslist.size(); i++) {
                balloonslist.get(i).setDirection(Direction.Up);
            }
            timer.start();
            // set direction to down
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            for (int i = 0; i < balloonslist.size(); i++) {
                balloonslist.get(i).setDirection(Direction.Down);
            }
            timer.start();
            // set direction to left
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            for (int i = 0; i < balloonslist.size(); i++) {
                balloonslist.get(i).setDirection(Direction.Left);
            }
            timer.start();
            // set direction to right
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            for (int i = 0; i < balloonslist.size(); i++) {
                balloonslist.get(i).setDirection(Direction.Right);
            }
            timer.start();
        } else if (e.getKeyCode() == KeyEvent.VK_S) {
            timer.stop();
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            balloonslist.add(new Balloon(((int)(Math.random()*800)),((int)(Math.random()*800))));
            repaint();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}