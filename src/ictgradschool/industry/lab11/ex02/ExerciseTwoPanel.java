package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    // Declare buttons and textfields as instance variables
    private JButton addButton;
    private JButton subtractButton;
    private JTextField userInput1;
    private JTextField userInput2;
    private JTextField calculatedResults;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        // Construct buttons and textfields
        addButton = new JButton("Add");
        subtractButton = new JButton("Subtract");
        userInput1 = new JTextField(10);
        userInput2 = new JTextField(10);
        calculatedResults = new JTextField(20);

        // Declare and construct labels
        JLabel resultsLabel = new JLabel("Results: ");

        // Add everything to window
        this.add(userInput1);
        this.add(userInput2);

        this.add(addButton);
        this.add(subtractButton);

        this.add(resultsLabel);
        this.add(calculatedResults);

        // Add action listeners
        addButton.addActionListener(this);
        subtractButton.addActionListener(this);

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // implement method

        double input1 = Double.parseDouble(userInput1.getText());
        double input2 = Double.parseDouble(userInput2.getText());

        if (e.getSource() == addButton) {
            calculatedResults.setText("" + roundTo2DecimalPlaces(input1+input2));
        }
        else if (e.getSource() == subtractButton) {
            calculatedResults.setText("" + roundTo2DecimalPlaces(input1-input2));
        }

    }
}